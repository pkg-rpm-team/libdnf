libdnf (0.74.0-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/0.74.0'
  * Refresh patch to remove fuzz from new version
  * Drop 0015-fix-segfault.patch, merged upstream

 -- Luca Boccassi <bluca@debian.org>  Sat, 08 Mar 2025 12:29:37 +0000

libdnf (0.73.4-2) unstable; urgency=medium

  * Backport patch to fix libdnf segfault

 -- Luca Boccassi <bluca@debian.org>  Mon, 27 Jan 2025 11:16:37 +0000

libdnf (0.73.4-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/0.73.4'
  * Refresh patches to remove fuzz

 -- Luca Boccassi <bluca@debian.org>  Thu, 14 Nov 2024 12:17:18 +0000

libdnf (0.73.3-1) unstable; urgency=high

  * Drop unused Valgrind bits
  * Update upstream source from tag 'upstream/0.73.3'
  * Update patches to remove fuzz from 0.73.3
  * Bump dependency on librepo to avoid libselinux crash

 -- Luca Boccassi <bluca@debian.org>  Mon, 26 Aug 2024 16:44:33 +0100

libdnf (0.73.2-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/0.73.2'

 -- Luca Boccassi <bluca@debian.org>  Sat, 29 Jun 2024 20:38:56 +0100

libdnf (0.73.1-3) unstable; urgency=medium

  * Remove workaround for dh_python3 regression, fixed in latest upload
  * Drop patch to use rpmdb in home, no longer needed

 -- Luca Boccassi <bluca@debian.org>  Mon, 06 May 2024 20:29:03 +0100

libdnf (0.73.1-2) unstable; urgency=medium

  * Add workaround for dh_python3 regression that wrongly renames cpython
    module

 -- Luca Boccassi <bluca@debian.org>  Sat, 20 Apr 2024 20:17:09 +0100

libdnf (0.73.1-1) unstable; urgency=medium

  * Switch to git-buildpackage workflow
  * Update upstream source from tag 'upstream/0.73.1'
  * Drop 0006-Add-rpmdb-in-home-directory-to-checksum-and-DNF-cont.patch,
    no longer needed
  * Refresh patches for new version
  * Bump dependency on librepo-dev to 1.15.0
  * Bump dependency on rpm to 4.15.0
  * Bump dependency on modulemd to 2.11.2
  * python3-libdnf: install python metadata too
  * Drop dh_missing from d/rules, it's default in compat 13
  * Drop dependency on python3-nose, deprecated (Closes: #1018395)
  * Drop transitional libdnf1 package (Closes: #1038262)
  * python3-hawkey-doc: add dependencies on libjs-jquery and libjs-
    underscore (Closes: #988770)
  * Add d/clean to remove leftover doc files (Closes: #1045949)
  * 0013-python-tests-fix-locale-issues.patch: do not create
    python/hawkey/tests/tests/__init__.py
  * Use a patch instead of manually mangling source file (Closes: #1045949)
  * Switch to pkgconf
  * Set Rules-Requires-Root: no
  * Switch to dh-sequence-python3
  * Enable ELF metadata stamping
  * Bump Standards-Version to 4.7.0
  * Move manpage to -dev package
  * Fix typo in 0013-python-tests-fix-locale-issues.patch
  * Drop Lintian override for python3-hawkey, no longer needed

 -- Luca Boccassi <bluca@debian.org>  Mon, 15 Apr 2024 01:03:35 +0100

libdnf (0.69.0-2.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.

 -- Steve Langasek <vorlon@debian.org>  Wed, 31 Jan 2024 22:13:42 +0000

libdnf (0.69.0-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Use mkdir -p in override_dh_auto_test to fix FTBFS on buildds

 -- Luca Boccassi <bluca@debian.org>  Fri, 03 Nov 2023 19:22:17 +0000

libdnf (0.69.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Move python3 modules from /usr/lib/local/ (Closes: #1052915)

 -- Luca Boccassi <bluca@debian.org>  Sun, 29 Oct 2023 15:57:28 +0000

libdnf (0.69.0-2) unstable; urgency=medium

  * Set myself to maintainer

 -- Frédéric Pierret <frederic.pierret@qubes-os.org>  Sun, 08 Jan 2023 10:08:51 +0100

libdnf (0.69.0-1) unstable; urgency=medium

  * Update to libdnf-0.69.0.
  * Update debian/control:bump debhelper-compat to 13 and update
    Build-Depends constraints.
  * Update debian/rules: remove reproducible flag being the default,
    remove not needed anymore PYTHON_DESIRED from dh_auto_configure,
    remove undeeded override of dh_install as debhelper is bumped and,
    few cleanups.
  * Update debian/watch: update to use tags endpoint.

 -- Frédéric Pierret <frederic.pierret@qubes-os.org>  Sat, 12 Nov 2022 15:51:20 +0100

libdnf (0.62.0-1) experimental; urgency=high

  * Update to libdnf-0.62.0

 -- Frédéric Pierret <frederic.pierret@qubes-os.org>  Wed, 14 Apr 2021 14:59:34 +0200

libdnf (0.55.2-5) unstable; urgency=medium

  * Team upload.

  [ Marek Marczykowski-Górecki ]
  * Use shlibs file instead of symbols.

 -- Holger Levsen <holger@debian.org>  Thu, 04 Feb 2021 01:17:07 +0100

libdnf (0.55.2-4) unstable; urgency=medium

  * Team upload.
  * Source upload for testing migration.

 -- Holger Levsen <holger@debian.org>  Wed, 03 Feb 2021 13:05:37 +0100

libdnf (0.55.2-3) unstable; urgency=medium

  * debian/copyright: Update and specify copyright holders

 -- Frédéric Pierret <frederic.pierret@qubes-os.org>  Tue, 02 Feb 2021 14:13:30 +0100

libdnf (0.55.2-2) unstable; urgency=medium

  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove listed license files (COPYING) from copyright.
  * Remove unnecessary get-orig-source-target.

 -- Frédéric Pierret <frederic.pierret@qubes-os.org>  Wed, 30 Dec 2020 09:38:25 -0000

libdnf (0.55.2-1) unstable; urgency=medium

  * Initial upload.
  * libdnf, libcomps and librepo are all needed for dnf, see #978627 (ITP bug
    for dnf) and https://github.com/QubesOS/qubes-issues/issues/5940 for more
    details on this packaging efford.

 -- Frédéric Pierret <frederic.pierret@qubes-os.org>  Sat, 26 Dec 2020 10:00:00 +0100
